class Pickaxe {

  int cubeSize = 35; 
  int xPos = 100;
  int yPos = 470 - cubeSize;

  int activeKey = 0;

  int[][] drawing = {{0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0}, 
    {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0}, 
    {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0}, 
    {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0}, 
    {0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1}, 
    {0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1}, 
    {0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1}, 
    {0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1}, 
    {0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1}, 
    {0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0}, 
    {0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
    {1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, 
    {1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}};

  public int[][] buttonPositions = {{xPos + cubeSize * 5-13, yPos - cubeSize * 5 - 13}, 
    {xPos + cubeSize * 4-13, yPos - cubeSize * 4 - 13},
    
    {xPos + cubeSize * 7-7, yPos - cubeSize * 3},
    {xPos + cubeSize * 7-7, yPos - cubeSize * 1 - 4},
    {xPos + cubeSize * 6, yPos - cubeSize * 2-7+12},
    {xPos + cubeSize * 8-13, yPos - cubeSize * 2-7},
    
    {xPos + cubeSize * 13-7, yPos - cubeSize * 10},
    {xPos + cubeSize * 13-7, yPos - cubeSize * 8 - 4},
    {xPos + cubeSize * 12, yPos - cubeSize * 9-7+12},
    {xPos + cubeSize * 14-13, yPos - cubeSize * 9-7},
    
    {xPos + cubeSize * 14-13, yPos - cubeSize * 6-7}
  

};

  // x, y, radius
  public int[] thumbPosition = {xPos + cubeSize * 7, yPos - cubeSize * 2, cubeSize}; 

  // buttonA, buttonB, thumbUp, thumbDown, thumbLeft, thumbRight, accUp, accDown, accLeft, accRight, accWhip
  String[] mappings = {"?", "?", "?", "?", "?", "?", "?", "?", "?", "?", "?"};
  
  int[] mappingCodes = {1,2,3,4,5,6,7,8,9,10,11};

  Pickaxe() {
  }

  void SetMapping(int keyIndex, String val) {
    mappings[keyIndex] = val;
  }

  void SetMapping(String val, int mappingCode) {
    SetMapping(activeKey, val);
    SetMappingCode(activeKey, mappingCode);
  }
    
  void SetMappingCode(int idx, int val){
    mappingCodes[idx] = val;
    
    
    //println("Mapping Codes");
    //for (int i = 0; i < 11; i++){
    // print(mappingCodes[i]);
    // print(", ");
    //}
    //println();
  }

  void Draw() {
    
    fill(255,255,255);
    ellipse(thumbPosition[0], thumbPosition[1], thumbPosition[2], thumbPosition[2]);
    
    

    fill(255, 255, 255);
    for (int x = 0; x < 13; x++) {
      for (int y = 0; y < 13; y++) {
        if (drawing[x][y] == 1) {
          rect(xPos + cubeSize * (12-x), yPos - cubeSize * y, cubeSize, cubeSize);
        }
      }
    }

    textAlign(RIGHT);
    for (int i = 0; i < 11; i++) {
      fill(0, 0, 0);
      text(mappings[i], buttonPositions[i][0]+10, buttonPositions[i][1]-1);

      if (Contains(buttonPositions[i][0], buttonPositions[i][1], 13, 13, mouseX, mouseY)) {
        fill(255, 0, 0);
      } else {
        fill(0, 255, 0);
      }

      rect(buttonPositions[i][0], buttonPositions[i][1], 13, 13);
    }

    fill(0,0,0);
    text("ThumbStick", thumbPosition[0] + cubeSize, thumbPosition[1] + cubeSize * 2);
    text("Pan/Tilt", xPos + cubeSize * 13+12, yPos - cubeSize * 11);
    
    text("Whip", xPos + cubeSize * 14+12, yPos - cubeSize * 5-12);
  }
}
