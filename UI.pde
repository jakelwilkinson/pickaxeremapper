import controlP5.*;
import java.util.*;

ControlP5 cp5;

float lastUpdateTime = -10000;

String[] mappings = {"?", "MouseXAxis", "MouseYAxis", "MouseLeftButton", "MouseMiddleButton", "MouseRightButton", 
  "KEY_LEFT_CTRL", "KEY_LEFT_SHIFT", "KEY_LEFT_ALT", "KEY_LEFT_GUI", "KEY_RIGHT_CTRL", 
  "KEY_RIGHT_SHIFT", "KEY_RIGHT_ALT", "KEY_RIGHT_GUI", "KEY_UP_ARROW", "KEY_DOWN_ARROW", 
  "KEY_LEFT_ARROW", "KEY_RIGHT_ARROW", "KEY_BACKSPACE", "KEY_TAB", "KEY_RETURN", "KEY_ESC", 
  "KEY_INSERT", "KEY_DELETE", "KEY_PAGE_UP", "KEY_PAGE_DOWN", "KEY_HOME", "KEY_END", 
  "KEY_CAPS_LOCK", "KEY_F1", "KEY_F2", "KEY_F3", "KEY_F4", "KEY_F5", "KEY_F6", "KEY_F7", 
  "KEY_F8", "KEY_F9", "KEY_F10", "KEY_F11", "KEY_F12", 
  "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", 
  "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", };

int[] mouseCodes    = {0, 8, 9, 1, 4, 2};  //0=null, 8=mouseX, 9=mouseY

int[] keyboardCodes = {0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0xDA, 0xD9, 0xD8, 0xD7, 0xB2, 0xB3, 0xB0, 0xB1, 0xD1, 0xD4, 0xD3, 0xD6, 0xD2, 0xD5, 
  0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xCB, 0xCC, 0xCD, 0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 
  0XF9, 0xFA, 0xFB}; //6 -> 6+47

boolean UIInitiated = false;

String GetMappingLabel(int mappingCode) {
  for (int i = 0; i < mouseCodes.length; i++) {
    if (mouseCodes[i] == mappingCode) {
      return mappings[i];
    }
  }
  for (int i = 0; i < keyboardCodes.length; i++) {
    if (keyboardCodes[i] == mappingCode) {
      return mappings[i+6];
    }
  }
  return str(parseChar(mappingCode));
}

void initUI() {
  println("REAL INIT");
  cp5 = new ControlP5(this);
  List l = Arrays.asList(mappings);
  /* add a ScrollableList, by default it behaves like a DropdownList */

  cp5.addScrollableList("ButtonMapping")
    .setPosition(400, 100)
    .setSize(200, 340)
    .setBarHeight(20)
    .setItemHeight(20)
    .addItems(l)
    .setType(ScrollableList.LIST) // currently supported DROPDOWN and LIST
    .setOpen(true)
    .setValue(0);  

  cp5.addButton("Apply")
    .setPosition(width - 110, height - 24 - 10)
    .setSize(100, 24)
    ;

  cp5.getController("ButtonMapping").hide();
  UIInitiated = true;
}

void uiDraw() {
  if (millis() - lastUpdateTime < 5000){
     text("Updated Successfully", 150, 20); 
  }
}

void mousePressed() {

  if (mouseButton == LEFT) {
    for (int i = 0; i < 11; i++) {
      if (Contains(p.buttonPositions[i][0], p.buttonPositions[i][1], 13, 13, mouseX, mouseY)) {
        println("Clicked " + i);
        p.activeKey = i;
        cp5.getController("ButtonMapping").show();
      } else {
      }
    }
  } else if (mouseButton == RIGHT) {
    cp5.getController("ButtonMapping").hide();
  }
}

void controlEvent(ControlEvent theEvent) {
  //println(theEvent.getController().getName());
}

public void Apply() {  
  println("Sending new mappings to controller...");
  serial.write('*');
  for (int i = 0; i < p.mappingCodes.length; i++) {
    serial.write(p.mappingCodes[i]);
  }
  serial.write('/');
  serial.write('&');  
  lastUpdateTime = millis();
}

void ButtonMapping(int n) {
  println(n, cp5.get(ScrollableList.class, "ButtonMapping").getValue());
  p.SetMapping(mappings[n], GetMappingCode(n));
  cp5.getController("ButtonMapping").hide();
}

int GetMappingCode(int idx) {
  if (idx < 6) {
    return mouseCodes[idx];
  } else if (idx < 41){
    return keyboardCodes[idx-6];
  } else {
    return parseChar(mappings[idx].charAt(0));    
  }
}



boolean Contains(int x, int y, int w, int h, int pX, int pY) {
  if (pX < x || pX > x+w) return false;
  if (pY < y || pY > y+h) return false;

  return true;
}
