import processing.serial.*;

Pickaxe p = new Pickaxe();

Serial serial;

boolean receivingConfig = false;
int configCount = 0;

void setup() {
  size(640, 480);

  printArray(Serial.list());


  //initUI();
  
  //for (int i = 0; i < mappings.length; i++){
  //  print(i + ": " + mappings[i] + ",   " + GetMappingCode(i) + "\n");
  //}
}


void draw() {
  background(200);
  
  if (UIInitiated) {
    p.Draw(); 
    uiDraw();
  } else if (serial == null) {
    fill(0, 0, 0);
    text("Press key to connect to serial port: (0-9)", 50, 50-24);
    for (int i = 0; i < Serial.list().length; i++) {
      text(i + ": " + Serial.list()[i], 50, 50 + 24*i);
    }
  } else {
  }
  

  if (serial != null) {
    while (serial.available() > 0) {
      int inByte = serial.read();
      processSerial(inByte);
    }
  }
}

void processSerial(int incomingByte) {

  //print(char(incomingByte));
  //println();
  //int incomingByte = -1;


  if (incomingByte == 42 && !receivingConfig) {
    receivingConfig = true;
    configCount = -1;
  }
  
  if (receivingConfig){
    println(configCount + ":  " + incomingByte);
  }

  if (receivingConfig) {
    if (incomingByte == '@') {
      receivingConfig = false;
      configCount=0;
      println("Config received");
      println(p.mappings[10]);
      if (!UIInitiated) {
        println("INIT");
        initUI();
      }
    } else if (configCount >= 0) {
      println("Set mapping for " + configCount + ": " + GetMappingLabel(incomingByte) + ", " + incomingByte); 
      p.activeKey = configCount;
      p.SetMapping(GetMappingLabel(incomingByte), incomingByte);
    }


    configCount++;
  }
}

void keyPressed() {
  if (serial == null) {
    for (int i = 0; i < Serial.list().length; i++) {
      if (Integer.valueOf(key)-48 == i) {
        println("Connecting to " + Serial.list()[i] + "...");
        serial = new Serial(this, Serial.list()[i], 115200);
        serial.write('&'); // Request config from device
      }
    }
  } else {
    if (key == 's') {
      serial.write('*');
      for (int i = 0; i < p.mappingCodes.length; i++) {
        serial.write(p.mappingCodes[i]);
      }
      serial.write('/');
    }
  }
}
