# RealRobots USB Pickaxe Remapper

Firmware at https://gitlab.com/jakelwilkinson/pickaxe_firmware

### Installation

To run this software you'll first need to download the Processing IDE from https://processing.org/download/

After installing Processing, run it and use it to open the PickaxeRemapper.pde file in this repo.



### Use

When run the Pickaxe Remapper will provide a list of connected USB peripherals. Press the number key on your keyboard for the correct device.

If your device isn't on the list check to make sure it's plugged in correctly.



![chooseport](https://gitlab.com/jakelwilkinson/pickaxeremapper/-/raw/master/img/chooseport.png)

In the above case you would press the '1' button on the keyboard.



![setcontrols](https://gitlab.com/jakelwilkinson/pickaxeremapper/-/raw/master/img/setcontrols.png)

Press the green buttons to assign keys and mouse axis. Above are the default controls.

Press "Apply" to send the updated control scheme to the USB Pickaxe.

It should immediately start using the new control scheme.



### Known Bugs

- Not every possible control is working, especially with mouse axis and alternate controls for the Pan/Tilt.
- When connecting, the current control scheme is uploaded to the software, this isn't currently working for the "Whip" control (flicking the controller back and forth) so it will have to be manually reentered on every connection.



